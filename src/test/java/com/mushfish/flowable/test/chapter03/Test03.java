package com.mushfish.flowable.test.chapter03;

import org.flowable.engine.*;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.Test;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipInputStream;

public class Test03 {

    @Test
    public void processEngine01() {
        // 获取  ProcessEngineConfiguration 对象
        ProcessEngineConfiguration configuration = new StandaloneProcessEngineConfiguration();
        // 配置 相关的数据库的连接信息
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("123456");
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/flowable-learn?serverTimezone=UTC&nullCatalogMeansCurrent=true");
        // 如果数据库中的表结构不存在就新建
        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        ProcessEngine processEngine = configuration.buildProcessEngine();
    }

    /**
     * 加载默认的配置文件
     */
    @Test
    public void processEngin02() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println("defaultProcessEngine = " + defaultProcessEngine);
    }

    /**
     * 加载自定义名称的配置文件
     */
    @Test
    public void processEngin03() {
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration
                .createProcessEngineConfigurationFromResource("flowable.cfg.xml");
        ProcessEngine processEngine = configuration.buildProcessEngine();
        System.out.println("processEngine = " + processEngine);
    }


    /**
     * 获取服务
     */
    @Test
    public void getService() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        // 获取运行时服务 RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        // 获取仓库服务 RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 获取任务服务 TaskService
        TaskService taskService = processEngine.getTaskService();
        // 获取管理服务 ManagementService
        ManagementService managementService = processEngine.getManagementService();
        // 获取身份认证服务 IdentityService
        IdentityService identityService = processEngine.getIdentityService();
        // 获取历史服务 HistoryService
        HistoryService historyService = processEngine.getHistoryService();
        // 获取表单服务 FormService
        FormService formService = processEngine.getFormService();
        // 获取动态BPMN服务 DynamicBpmnService
        DynamicBpmnService dynamicBpmnService = processEngine.getDynamicBpmnService();

        System.out.println("Runtime Service: " + runtimeService);
        System.out.println("Repository Service: " + repositoryService);
        System.out.println("Task Service: " + taskService);
        System.out.println("Management Service: " + managementService);
        System.out.println("Identity Service: " + identityService);
        System.out.println("History Service: " + historyService);
        System.out.println("Form Service: " + formService);
        System.out.println("Dynamic BPMN Service: " + dynamicBpmnService);
    }

    /**
     * 部署流程
     */
    @Test
    public void testDeploy() {
        // 1.获取 ProcessEngine 对象
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        // 2.获取RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 3.完成流程的部署操作
        Deployment deploy = repositoryService.createDeployment()// 创建Deployment对象
                .addClasspathResource("myProcess.bpmn20.xml") // 添加流程部署文件
                .name("请假流程") // 设置部署流程的名称
                .deploy(); // 执行部署操作
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
    }

    /**
     * 流程定义挂起和激活
     */
    @Test
    public void testProcessDefinitionActivateAndSuspend() {
        // 获取流程引擎对象
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId("myProcess:5:155004")
                .singleResult();
        // 获取流程定义的状态
        boolean suspended = processDefinition.isSuspended();
        System.out.println("suspended = " + suspended);
        if (suspended) {
            // 表示被挂起
            System.out.println("激活流程定义");
            repositoryService.activateProcessDefinitionById("myProcess:5:155004", true, null);
        } else {
            // 表示激活状态
            System.out.println("挂起流程");
            repositoryService.suspendProcessDefinitionById("myProcess:5:155004", true, null);
        }
    }

    /**
     * 启动流程实例
     */
    @Test
    public void testRunProcess() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        // 我们需要通过RuntimeService来启动流程实例
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // 启动流程实例
        ProcessInstance holidayRequest = runtimeService.startProcessInstanceById("myProcess:5:155004"); //从库中act_re_procdef表查看该值
        System.out.println("holidayRequest.getProcessDefinitionId() = " + holidayRequest.getProcessDefinitionId());
        System.out.println("holidayRequest.getActivityId() = " + holidayRequest.getActivityId());
        System.out.println("holidayRequest.getId() = " + holidayRequest.getId());
    }

    /**
     * 流程实例挂起和激活
     */
    @Test
    public void testProcessInstanceActivateAndSuspend() {
        // 1.获取ProcessEngine对象
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2.获取RuntimeService
        RuntimeService runtimeService = engine.getRuntimeService();
        // 3.获取流程实例对象
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId("162501")
                .singleResult();
        // 4.获取相关的状态操作
        boolean suspended = processInstance.isSuspended();
        String id = processInstance.getId();
        if (suspended) {
            // 挂起--》激活
            runtimeService.activateProcessInstanceById(id);
            System.out.println("流程实例：" + id + "，已激活");
        } else {
            // 激活--》挂起
            runtimeService.suspendProcessInstanceById(id);
            System.out.println("流程实例：" + id + "，已挂起");
        }
    }

    /**
     * 完成zhangsan当前任务
     */
    @Test
    public void testCompleteTask() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess")
                .taskAssignee("zhangsan")
                .list();

        for (Task task : list) {
            // 完成任务
            taskService.complete(task.getId());
        }
    }
}
