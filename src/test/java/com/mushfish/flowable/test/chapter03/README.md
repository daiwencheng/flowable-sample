## 1.创建ProcessEngine
### 使用编码创建
```java
    @Test
    public void processEngine01(){
        // 获取  ProcessEngineConfiguration 对象
        ProcessEngineConfiguration configuration = new StandaloneProcessEngineConfiguration();
        // 配置 相关的数据库的连接信息
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("123456");
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/flowable-learn?serverTimezone=UTC&nullCatalogMeansCurrent=true");
        // 如果数据库中的表结构不存在就新建
        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        ProcessEngine processEngine = configuration.buildProcessEngine();
    }
```
### 使用配置文件创建
1. 在resources下创建`flowable.cfg.xml`文件
```xml
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <bean id="processEngineConfiguration"
          class="org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration">
        <property name="jdbcUrl" value="jdbc:mysql://localhost:3306/flowable-learn?allowMultiQueries=true&amp;useUnicode=true&amp;characterEncoding=UTF-8&amp;useSSL=false&amp;serverTimezone=UTC&amp;nullCatalogMeansCurrent=true" /><property name="jdbcDriver" value="com.mysql.cj.jdbc.Driver" />
        <property name="jdbcUsername" value="root" />
        <property name="jdbcPassword" value="123456" />
        <property name="databaseSchemaUpdate" value="true" />
        <property name="asyncExecutorActivate" value="false" />
    </bean>
</beans>
```
2. 编码加载配置文件，创建ProcessEngine
```java
    /**
     * 加载默认的配置文件
     */
    @Test
    public void processEngin02(){
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        System.out.println("defaultProcessEngine = " + defaultProcessEngine);
    }

    /**
     * 加载自定义名称的配置文件
     */
    @Test
    public void processEngin03() {
        ProcessEngineConfiguration configuration = ProcessEngineConfiguration
                .createProcessEngineConfigurationFromResource("flowable.cfg.xml");
        ProcessEngine processEngine = configuration.buildProcessEngine();
        System.out.println("processEngine = " + processEngine);
    }
```
## 2.获取service
```java
    /**
     * 获取服务
     */
    @Test
    public void getService() {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        // 获取运行时服务 RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();
        // 获取仓库服务 RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 获取任务服务 TaskService
        TaskService taskService = processEngine.getTaskService();
        // 获取管理服务 ManagementService
        ManagementService managementService = processEngine.getManagementService();
        // 获取身份认证服务 IdentityService
        IdentityService identityService = processEngine.getIdentityService();
        // 获取历史服务 HistoryService
        HistoryService historyService = processEngine.getHistoryService();
        // 获取表单服务 FormService
        FormService formService = processEngine.getFormService();
        // 获取动态BPMN服务 DynamicBpmnService
        DynamicBpmnService dynamicBpmnService = processEngine.getDynamicBpmnService();

        System.out.println("Runtime Service: " + runtimeService);
        System.out.println("Repository Service: " + repositoryService);
        System.out.println("Task Service: " + taskService);
        System.out.println("Management Service: " + managementService);
        System.out.println("Identity Service: " + identityService);
        System.out.println("History Service: " + historyService);
        System.out.println("Form Service: " + formService);
        System.out.println("Dynamic BPMN Service: " + dynamicBpmnService);
    }
```

1. **RuntimeService：** 提供了与运行时流程实例相关的服务，如启动流程实例、查询任务等。

2. **RepositoryService：** 提供了与流程定义仓库相关的服务，如部署、查询和删除流程定义等。

3. **TaskService：** 提供了与任务相关的服务，如查询、完成和委托任务等。

4. **ManagementService：** 提供了与引擎管理相关的服务，如查询引擎表和作业等。

5. **IdentityService：** 提供了与用户和组相关的服务，如创建、查询和删除用户等。

6. **HistoryService：** 提供了与历史数据相关的服务，如查询流程实例历史、任务历史等。

7. **FormService：** 提供了与表单相关的服务，如提交表单和获取表单数据等。

8. **DynamicBpmnService：** 提供了与动态BPMN相关的服务，如修改流程定义的BPMN XML等。

## 3.图标
### 事件图标
在Flowable中的事件图标`启动事件`、`边界事件`、`中间事件`和`结束事件`
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/d96e2625b558421e909d9ca17a228995.png)
1. **启动事件（Start Event）:**
   - **定义：** 启动事件是流程实例的开始点。在 Flowable 中，启动事件用于指示何时开始一个新的流程实例。
   - **用法：** 一个流程通常从一个启动事件开始。Flowable 支持不同类型的启动事件，如消息启动、定时启动、信号启动等，具体取决于流程的需要。

2. **边界事件（Boundary Event）:**
   - **定义：** 边界事件与某个任务或子流程关联，它们位于这些任务或子流程的边界上。当特定条件满足时，边界事件可以中断或补充当前任务的执行。
   - **用法：** 用于在任务执行期间处理特定的中断条件或补充行为。边界事件通常与定时器、消息、错误等事件类型关联。

3. **中间事件（Intermediate Event）:**
   - **定义：** 中间事件用于在流程执行的中间插入某些操作或等待某些条件的发生。它们不是流程的开始或结束点，而是流程执行过程中的事件点。
   - **用法：** 用于捕捉某些事件、执行某些操作或等待某些条件。中间事件可以与定时器、消息、信号等事件类型关联。

4. **结束事件（End Event）:**
   - **定义：** 结束事件是流程实例的结束点。在 Flowable 中，结束事件用于指示何时结束一个流程实例。
   - **用法：** 一个流程通常以一个结束事件结束。Flowable 支持不同类型的结束事件，如消息结束、信号结束等，具体取决于流程的需要。

### 活动(任务)图标
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/8d996c36418e48c6b40f03b98cc9258d.png)
在 Flowable 中，活动（Activity）或任务（Task）是流程中的执行单元，代表了工作流程中的一个步骤或操作。活动可以是用户任务、自动任务、服务任务等，它们在流程中定义了工作的具体内容。

### 结构图标
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/99b1a2fcc4a94f9099182c2be675d461.png)
结构图标可以看做是整个流程活动的结构，一个流程中可以包括子流程。

### 网关图标
网关用来处理决策
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/df80919d90b64c62aaec6dfb1ec85509.png)
## 4.部署流程
```java
    /**
     *部署流程
     */
    @Test
    public void testDeploy(){
        // 1.获取 ProcessEngine 对象
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        // 2.获取RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 3.完成流程的部署操作
        Deployment deploy = repositoryService.createDeployment()// 创建Deployment对象
                .addClasspathResource("myProcess.bpmn20.xml") // 添加流程部署文件
                .name("请假流程") // 设置部署流程的名称
                .deploy(); // 执行部署操作
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
    }
```
## 5.流程定义挂起和激活
- 部署的流程默认的状态为激活
- 如果我们暂时不想使用该定义的流程，那么可以挂起该流程。
- 流程定义为挂起状态，该流程定义将不允许启动新的流程实例，同时该流程定义下的所有的流程实例都将全部挂起暂停执行。
```java
    /**
     * 流程定义挂起和激活
     */
    @Test
    public void testProcessDefinitionActivateAndSuspend() {
        // 获取流程引擎对象
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                .processDefinitionId("myProcess:5:155004")
                .singleResult();
        // 获取流程定义的状态
        boolean suspended = processDefinition.isSuspended();
        System.out.println("suspended = " + suspended);
        if (suspended) {
            // 表示被挂起
            System.out.println("激活流程定义");
            repositoryService.activateProcessDefinitionById("myProcess:5:155004", true, null);
        } else {
            // 表示激活状态
            System.out.println("挂起流程");
            repositoryService.suspendProcessDefinitionById("myProcess:5:155004", true, null);
        }
    }
```

## 6.启动流程实例
```java
    /**
     * 启动流程实例
     */
    @Test
    public void testRunProcess(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        // 我们需要通过RuntimeService来启动流程实例
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // 启动流程实例
        ProcessInstance holidayRequest = runtimeService.startProcessInstanceById("myProcess:5:155004"); //从库中act_re_procdef表查看该值
        System.out.println("holidayRequest.getProcessDefinitionId() = " + holidayRequest.getProcessDefinitionId());
        System.out.println("holidayRequest.getActivityId() = " + holidayRequest.getActivityId());
        System.out.println("holidayRequest.getId() = " + holidayRequest.getId());
    }
```
## 7.流程实例挂起和激活
```java
    /**
     * 流程实例挂起和激活
     */
    @Test
    public void testProcessInstanceActivateAndSuspend() {
        // 1.获取ProcessEngine对象
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2.获取RuntimeService
        RuntimeService runtimeService = engine.getRuntimeService();
        // 3.获取流程实例对象
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId("162501")
                .singleResult();
        // 4.获取相关的状态操作
        boolean suspended = processInstance.isSuspended();
        String id = processInstance.getId();
        if (suspended) {
            // 挂起--》激活
            runtimeService.activateProcessInstanceById(id);
            System.out.println("流程实例：" + id + "，已激活");
        } else {
            // 激活--》挂起
            runtimeService.suspendProcessInstanceById(id);
            System.out.println("流程实例：" + id + "，已挂起");
        }
    }

```
## 8.执行任务
```java
    /**
     * 完成zhangsan当前任务
     */
    @Test
    public void testCompleteTask(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess")
                .taskAssignee("zhangsan")
                .list();

        for (Task task : list) {
            // 完成任务
            taskService.complete(task.getId());
        }
    }
```

