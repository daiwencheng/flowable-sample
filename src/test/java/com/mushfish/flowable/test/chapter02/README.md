## 1.安装IDEA插件`Flowable BPMN visualizer`
IDEA插件市场中搜索`Flowable BPMN visualizer`安装该插件
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/208dad1cfd2945759e9cffd3cae09a5e.png)
## 2.创建流程图
### 2.1.创建流程图文件
在resources下创建myProcess.bpmn20.xml流程图。
>创建时名称只用输入myProcess
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/ecfae640e0b14c17876a1c5376f98195.png)
### 2.2.创建流程图
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/9a8c5615f1d249f2a0e3f9386f393985.png)

1. 选中myProcess.bpmn20.xml，右键打开流程图设计界面
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/0bf515e7d95343f6b8cb3da67979b008.png)
2. 设计流程图
   流程名称和id
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/b7ace4e767664e40b1c77c7c46f7e3ff.png)
   创建开始事件
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/ce6b6a534a3140c9bb51b1222b25f31c.png)
   创建用户任务
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/43f5f70dac204b69bc69abfed91c4928.png)
   指定任务责任人`zhangsan`和`lisi`
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/a25fcc8d549c4922ae5183fefdbf97b9.png)
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/0abb113922024653aee23a5b6e9b60c6.png)
   创建结束事件
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/cc23101303df4086a5253ed92f13459d.png)
   连线
   ![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/d96b115074ef4c27a82e68216a1a0f65.png)
### 2.3.部署和执行流程
> 依次执行下列方法
```java
public class Test02 {

    ProcessEngineConfiguration configuration = null;
    @Before
    public void before(){
        // 获取  ProcessEngineConfiguration 对象
        configuration = new StandaloneProcessEngineConfiguration();
        // 配置 相关的数据库的连接信息
        configuration.setJdbcDriver("com.mysql.cj.jdbc.Driver");
        configuration.setJdbcUsername("root");
        configuration.setJdbcPassword("123456");
        configuration.setJdbcUrl("jdbc:mysql://localhost:3306/flowable-learn?serverTimezone=UTC&nullCatalogMeansCurrent=true");
        // 如果数据库中的表结构不存在就新建
        configuration.setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);
    }

    /**
     * 部署流程
     *
     */
    @Test
    public void testDeploy(){
        // 1.获取 ProcessEngine 对象
        ProcessEngine processEngine = configuration.buildProcessEngine();
        // 2.获取RepositoryService
        RepositoryService repositoryService = processEngine.getRepositoryService();
        // 3.完成流程的部署操作
        Deployment deploy = repositoryService.createDeployment()// 创建Deployment对象
                .addClasspathResource("myProcess.bpmn20.xml") // 添加流程部署文件
                .name("请假流程") // 设置部署流程的名称
                .deploy(); // 执行部署操作
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void testRunProcess(){
        ProcessEngine processEngine = configuration.buildProcessEngine();

        // 我们需要通过RuntimeService来启动流程实例
        RuntimeService runtimeService = processEngine.getRuntimeService();

        // 启动流程实例
        ProcessInstance holidayRequest = runtimeService.startProcessInstanceById("myProcess:4:145004"); //从库中act_re_procdef表查看该值
        System.out.println("holidayRequest.getProcessDefinitionId() = " + holidayRequest.getProcessDefinitionId());
        System.out.println("holidayRequest.getActivityId() = " + holidayRequest.getActivityId());
        System.out.println("holidayRequest.getId() = " + holidayRequest.getId());
    }

    /**
     * 查询和完成zhangsan任务
     */
    @Test
    public void testQueryTask(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess") // 指定查询的流程编程
                .taskAssignee("zhangsan") // 查询这个任务的处理人
                .list();
        for (Task task : list) {
            System.out.println("task.getProcessDefinitionId() = " + task.getProcessDefinitionId());
            System.out.println("task.getName() = " + task.getName());
            System.out.println("task.getAssignee() = " + task.getAssignee());
            System.out.println("task.getDescription() = " + task.getDescription());
            System.out.println("task.getId() = " + task.getId());
            taskService.complete(task.getId());
        }
    }

    /**
     * 完成lisi当前任务
     */
    @Test
    public void testCompleteTask(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess")
                .taskAssignee("lisi")
                .list();

        for (Task task : list) {
            // 完成任务
            taskService.complete(task.getId());
        }



    }

    /**
     * 获取流程任务的历史数据
     */
    @Test
    public void testHistory(){
        ProcessEngine processEngine = configuration.buildProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery()
                .processDefinitionId("myProcess:4:145004")
                .finished() // 查询的历史记录的状态是已经完成
                .orderByHistoricActivityInstanceEndTime().asc() // 指定排序的字段和顺序
                .list();
        for (HistoricActivityInstance history : list) {
            System.out.println(history.getActivityName()+":"+history.getAssignee()+"--"
                    +history.getActivityId()+":" + history.getDurationInMillis()+"毫秒");
        }

    }
}
```

> 另有flowable提供的流程设计器web应用`Flowable UI`可供选择