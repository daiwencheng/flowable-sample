>[课程来源B站大佬波哥的课程](https://www.bilibili.com/video/BV1oQ4y1J76o/)
> 
>本文仅做笔记，课件需要的联系B站大佬获取

### 大纲
#### 1. [Flowable基础入门](./src/test/java/com/mushfish/flowable/test/chapter01)
#### 2. [Flowable可视化流程设计器Flowable BPMN visualizer](./src/test/java/com/mushfish/flowable/test/chapter02)
#### 3. [Flowable基础API小结](./src/test/java/com/mushfish/flowable/test/chapter03)
